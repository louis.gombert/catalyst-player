#include <string>
#include <vector>

namespace CatalystPlayer {
int Play(std::vector<std::string> &args);
} // namespace CatalystPlayer