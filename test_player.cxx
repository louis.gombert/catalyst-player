#include "catalyst_player.h"

int main(int argc, char **argv) {
  std::vector<std::string> args(argv, argv + argc);
  CatalystPlayer::Play(args);

  return 1;
}