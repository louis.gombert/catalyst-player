#include "catalyst_adaptor.h"

#include <filesystem>
#include <iostream>
#include <limits>
#include <thread>
#include <vector>

#include <vtkDataSet.h>
#include <vtkNew.h>
#include <vtkXMLImageDataReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtksys/SystemTools.hxx>

namespace
{
template <class TReader>
vtkSmartPointer<vtkDataSet> ReadVtkXMLFile(std::string fileName)
{
  vtkNew<TReader> reader;
  reader->SetFileName(fileName.c_str());
  reader->Update();

  vtkSmartPointer<vtkDataSet> dataset;
  dataset.TakeReference(vtkDataSet::SafeDownCast(reader->GetOutput()->NewInstance()));
  dataset->ShallowCopy(reader->GetOutput());

  return dataset;
}
}

namespace CatalystPlayer
{
int Play(std::vector<std::string>& args)
{
  int argc = args.size();
  // Parse arguments
  if (argc < 3 || argc > 7)
  {
    std::cout << "Wrong number of arguments. Usage : catalyst_player FILENAME_FORMAT "
                 "PYTHON_SCRIPT [TIMESTEP_START] [TIMESTEP_STOP] [SLEEP_MS] [ARGS]"
              << std::endl;
    return 1;
  }

  // Mandatory arguments
  std::string filenameFormat = args[1];
  std::string scriptPath = args[2];

  // Parse filename
  std::string prefix, suffix;
  unsigned short padding = 0;
  for (int i = 0; i < filenameFormat.size(); i++)
  {
    if (filenameFormat[i] == '#')
    {
      padding++;
    }
    else if (padding == 0)
    {
      prefix += filenameFormat[i];
    }
    else
    {
      suffix += filenameFormat[i];
    }
  }

  // Optional arguments
  unsigned int timestepStart = 0;
  unsigned int timestepStop = std::numeric_limits<unsigned int>::max();
  unsigned int sleepMs = 0;
  if (argc > 3)
  {
    timestepStart = std::stoi(args[3]);
    if (argc > 4)
    {
      timestepStop = std::stoi(args[4]);
      if (argc > 5)
      {
        sleepMs = std::stoi(args[5]);
      }
    }
  }

  // Initialize Catalyst, passing arguments to the script if provided
  if (argc == 7)
  {
    Adaptor::Initialize(scriptPath.c_str(), args[6]);
  }
  else
  {
    Adaptor::Initialize(scriptPath.c_str());
  }

  for (unsigned int timestep = timestepStart; timestep <= timestepStop; timestep++)
  {
    std::stringstream ss_filename;
    ss_filename << prefix << std::setw(padding) << std::setfill('0') << timestep << suffix;
    std::string filename = ss_filename.str();

    if (!std::filesystem::exists(filename))
    {
      std::cout << "File " << filename << " not found, exiting" << std::endl;
      break;
    }

    std::cout << "Loading " << filename << std::endl;

    vtkSmartPointer<vtkDataSet> dataset;
    std::string extension = vtksys::SystemTools::GetFilenameLastExtension(filename);

    if (extension == ".vti")
    {
      dataset = ReadVtkXMLFile<vtkXMLImageDataReader>(filename);
    }
    else if (extension == ".vtr")
    {
      dataset = ReadVtkXMLFile<vtkXMLRectilinearGridReader>(filename);
    }
    else if (extension == ".vtu")
    {
      dataset = ReadVtkXMLFile<vtkXMLUnstructuredGridReader>(filename);
    }
    else
    {
      vtkErrorWithObjectMacro(nullptr, << args[0] << " Unknown extension: " << extension);
      return EXIT_FAILURE;
    }

    bool result = Adaptor::SendTimestep(dataset, 60.0 * timestep, timestep);
    if (!result)
    {
      vtkErrorWithObjectMacro(nullptr, "Error while serializing dataset");
      return EXIT_FAILURE;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(sleepMs));
  }

  Adaptor::Finalize();

  return EXIT_SUCCESS;
}
} // namespace CatalystPlayer